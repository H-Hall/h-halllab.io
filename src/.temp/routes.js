export default [
  {
    path: "/about/",
    component: () => import(/* webpackChunkName: "page--src--pages--about-vue" */ "/mnt/c/Users/jphh/OneDrive/Documents/DarkPuzzle/h-hall.gitlab.io/src/pages/About.vue")
  },
  {
    name: "404",
    path: "/404/",
    component: () => import(/* webpackChunkName: "page--node-modules--gridsome--app--pages--404-vue" */ "/mnt/c/Users/jphh/OneDrive/Documents/DarkPuzzle/h-hall.gitlab.io/node_modules/gridsome/app/pages/404.vue")
  },
  {
    path: "/:title/",
    component: () => import(/* webpackChunkName: "page--src--templates--post-vue" */ "/mnt/c/Users/jphh/OneDrive/Documents/DarkPuzzle/h-hall.gitlab.io/src/templates/Post.vue")
  },
  {
    name: "home",
    path: "/",
    component: () => import(/* webpackChunkName: "page--src--pages--index-vue" */ "/mnt/c/Users/jphh/OneDrive/Documents/DarkPuzzle/h-hall.gitlab.io/src/pages/Index.vue")
  },
  {
    name: "*",
    path: "*",
    component: () => import(/* webpackChunkName: "page--node-modules--gridsome--app--pages--404-vue" */ "/mnt/c/Users/jphh/OneDrive/Documents/DarkPuzzle/h-hall.gitlab.io/node_modules/gridsome/app/pages/404.vue")
  }
]

