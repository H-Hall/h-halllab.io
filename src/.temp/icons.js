export default {
  "touchiconMimeType": "image/png",
  "faviconMimeType": "image/png",
  "precomposed": false,
  "touchicons": [
    {
      "width": 76,
      "src": "/assets/static/src/favicon.png?width=76&key=68eadcb"
    },
    {
      "width": 152,
      "src": "/assets/static/src/favicon.png?width=152&key=68eadcb"
    },
    {
      "width": 120,
      "src": "/assets/static/src/favicon.png?width=120&key=68eadcb"
    },
    {
      "width": 167,
      "src": "/assets/static/src/favicon.png?width=167&key=68eadcb"
    },
    {
      "width": 180,
      "src": "/assets/static/src/favicon.png?width=180&key=68eadcb"
    }
  ],
  "favicons": [
    {
      "width": 16,
      "src": "/assets/static/src/favicon.png?width=16&key=25b6857"
    },
    {
      "width": 32,
      "src": "/assets/static/src/favicon.png?width=32&key=25b6857"
    },
    {
      "width": 96,
      "src": "/assets/static/src/favicon.png?width=96&key=25b6857"
    }
  ]
}